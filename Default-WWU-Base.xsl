
<!-- -*- mode: xml -*- -->
<!-- == Parametric Search Appliance 18.1.0 == $Id: default.xsl.src,v 1.70 2017/02/21 21:12:22 john Exp $ == thunderstone_file_sha1: 06c5b3a0b9929beb0c95ccf572e603f1743247ed -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:u="http://www.thunderstone.com/ParametricSearch/1.0/">

	<xsl:output method="html" doctype-system="about:legacy-compat"/>

	<xsl:preserve-space elements="Abstract"/>

	<xsl:variable name="GroupBySite" select="/ThunderstoneResults/Summary/GroupBySite"/>

	<!-- ===================================================================== -->
	<!-- overall search results page template -->
	<!-- ===================================================================== -->

	<xsl:template match="ThunderstoneResults">

		<xsl:choose>

			<xsl:when test="ProfileInfo/RedirectUrl != ''">
				<xsl:call-template name="RedirectPage"/>
			</xsl:when>

			<xsl:when test="ProfileInfo/LoginUrl != ''">
				<xsl:call-template name="LoginForm"/>
			</xsl:when>

			<xsl:otherwise>
				<xsl:call-template name="MainPage"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="MainPage">
		<!-- ===================================================================== -->
		<!-- WWU HEADER -->
		<!-- ===================================================================== -->
		<html class="">
			<head>
				<title>SEARCH</title>
				<meta charset="UTF-8"/>
				<!-- fonts -->
				<link rel="preconnect" href="https://fonts.googleapis.com" />
					<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="" />
						<link href="https://fonts.googleapis.com/css2?family%3DAtkinson%2BHyperlegible%3Aital%2Cwght%400%2C400%3B0%2C700%3B1%2C400%3B1%2C700%26family%3DFira%2BSans%2BExtra%2BCondensed%3Awght%40300%3B400%26family%3DFira%2BSans%3Aital%2Cwght%400%2C300%3B0%2C400%3B0%2C600%3B0%2C700%3B0%2C900%3B1%2C300%3B1%2C400%26family%3DMontserrat%3Awght%40700%3B900%26family%3DPT%2BSerif%3Awght%40400%3B700%26display%3Dswap" rel="stylesheet" />
							<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

								<!-- base styles -->
								<link rel="stylesheet" href="https://ashlar.blob.core.windows.net/ashlar-theme-files/css/normalize.css" />
								<link rel="stylesheet" href="https://ashlar.blob.core.windows.net/ashlar-theme-files/css/ashlar-base.css" />
								<link rel="stylesheet" type="text/css" href="https://www.wwu.edu/themes/contrib/ashlar/build/css/components.css"/>
								<link rel="stylesheet" href="https://ashlar.blob.core.windows.net/ashlar-theme-files/css/components/wwu-footer.css" />

								<!-- pre-header -->
								<link rel="stylesheet" href="https://ashlar.blob.core.windows.net/ashlar-theme-files/css/components/search.css" />
								<link rel="stylesheet" href="https://ashlar.blob.core.windows.net/ashlar-theme-files/css/components/pre-header.css" />
								
								<script src="https://ashlar.blob.core.windows.net/ashlar-theme-files/js/search.js"></script>
								<script src="https://ashlar.blob.core.windows.net/ashlar-theme-files/js/pre-header.js"></script>
								<wwu-pre-header class="black-bg"></wwu-pre-header>

								<!-- wwu-header -->
								<link rel="stylesheet" href="https://ashlar.blob.core.windows.net/ashlar-theme-files/css/components/wwu-header.css" />
								<script src="https://ashlar.blob.core.windows.net/ashlar-theme-files/js/logo.js"></script>
								<script src="https://ashlar.blob.core.windows.net/ashlar-theme-files/js/wwu-header.js"></script>
								<script src="https://ashlar.blob.core.windows.net/ashlar-theme-files/js/wwu-footer.js"></script>
								<wwu-header sitename="Search" regioncontent=""></wwu-header>
			</head>
			<body class="search2">
				<div class="layout-container">

					<div class="page">

					


						<main class="page-content" id="main-content">
							<div class="region-wrapper">
								<h1 class="page-title">
									<span property="schema:name">Search Results</span>
								</h1>
								<div class="region region--content">
									<div class="search-wrapper">
										<!-- ===================================================================== -->
										<!-- END HEADER -->
										<!-- ===================================================================== -->

										<xsl:call-template name="Messages"/>
										<div class='search'>

											<xsl:call-template name="SearchForm"/>

											<xsl:apply-templates select="TopBestBets"/>

											<xsl:apply-templates select="SiteQuery[.!='']"/>

											<xsl:call-template name="displayGroupBySelected"/>

											<xsl:apply-templates select="Summary/GroupByResults[Group/Count!='']"/>
											<h2 tabindex="-1" id="results-heading">Results</h2>

											<xsl:apply-templates select="Result"/>

											<xsl:variable name="hitsthispage" select="count(Result)"/>

											<xsl:if test="$hitsthispage>5">
												<xsl:apply-templates select="Summary"/>
											</xsl:if>

											<xsl:apply-templates select="Spelling"/>

											<xsl:apply-templates select="RightBestBets"/>
										</div>

										<!-- ===================================================================== -->
										<!-- WWU FOOTER -->
										<!-- ===================================================================== -->
									</div>
								</div>
							</div>
						</main>

						<footer role="contentinfo" class="page-footer region region--footer">
						 <wwu-footer></wwu-footer>
						



<!-- WWU FOOTER -->
							<script>
								window.onload = function () {
								document.getElementById("results-heading").focus();
								};
							</script>
						</footer>

					</div>
				</div>
			</body>
		</html>
		<!-- ===================================================================== -->
		<!-- END FOOTER -->
		<!-- ===================================================================== -->
	</xsl:template>

	<!-- ===================================================================== -->
	<!-- search form template -->
	<!-- ===================================================================== -->

	<xsl:template name="SearchForm">

		<xsl:call-template name="TestModeBanner"/>
		<form method="get" id="ThunderstoneSearchForm" action="{UrlRoot}/" class="ThunderstoneForm">
			<input type="hidden" name="mode" value="{mode}"/>
			<input type="hidden" name="opts" value="{opts}"/>
			<input type="hidden" name="pr" value="{Profile}"/>
			<input type="hidden" name="dropXSL" value="{dropXSL}"/>

			<xsl:for-each select="exportVar/variable[@propagate='true']">
				<input type="hidden" name="{@name}" VALUE="{.}"/>
			</xsl:for-each>

			<xsl:if test="AdvancedSearch!=1">

				<xsl:for-each select="InFieldQuery">
					<input type="hidden" id="infq{@field}" name="infq" value="{Query}"/>
				</xsl:for-each>
				<input type="hidden" id="sq" name="sq" value="{SiteQuery}"/>
				<input type="hidden" name="prox" value="{Proximity}"/>
				<input type="hidden" name="rorder" value="{RankOrder}"/>
				<input type="hidden" name="rprox" value="{RankProximity}"/>
				<input type="hidden" name="rdfreq" value="{RankDatabaseFrequency}"/>
				<input type="hidden" name="rwfreq" value="{RankDocumentFrequency}"/>
				<input type="hidden" name="rlead" value="{RankPosition}"/>
				<input type="hidden" name="rdepth" value="{RankDepth}"/>
				<input type="hidden" name="sufs" value="{Suffixes}"/>
				<input type="hidden" name="order" value="{Order}"/>

				<xsl:if test="ResultsPerSiteQuery != ''">
					<input type="hidden" name="sr" value="{ResultsPerSiteQuery}"/>
				</xsl:if>

				<xsl:if test="ParametricQuery/Query != ''">
					<input type="hidden" name="psq" value="{ParametricQuery/Query}"/>
				</xsl:if>

				<xsl:for-each select="ParametricQuery/Param">
					<input type="hidden" name="{@name}" value="{.}"/>
				</xsl:for-each>
			</xsl:if>

			<div class='simple-search'>
				<div class='search-area'>
					<input id="query" name="query" class="search-query queryAutocomplete" value="{TextQuery}" aria-label="Enter search terms."/>

					<xsl:if test="Category!='NULL'">
						<SELECT name="cq">

							<xsl:for-each select="Category">

								<xsl:if test="CatVisible='Y' or CatSel='selected'">
									<OPTION value="{CatVal}">

										<xsl:if test="CatSel='selected'">

											<xsl:attribute name='selected'>selected</xsl:attribute>
										</xsl:if>

										<xsl:value-of select="CatName"/>
									</OPTION>
								</xsl:if>
							</xsl:for-each>
						</SELECT>
					</xsl:if>

					<input type="submit" VALUE="Submit" class="search-submit"/>

				</div>
			</div>
		</form>
	</xsl:template>

	<!-- ===================================================================== -->
	<!-- search results spell check info template -->
	<!-- ===================================================================== -->

	<xsl:template match="Spelling">
		<div class='suggest-spelling'>

			<xsl:for-each select="SuggestWord">

				<xsl:if test="position()=1">
					<xsl:value-of select="SpellPhrase"/>
				</xsl:if>
				<a href="{SpellLink}" class="spelling-link">

					<xsl:value-of select="SpellWord"/>
					<xsl:text disable-output-escaping="yes"> (</xsl:text>

					<xsl:value-of select="SpellCount"/>
					<xsl:text disable-output-escaping="yes">)</xsl:text>
				</a>
			</xsl:for-each>
		</div>
	</xsl:template>

	<!-- ===================================================================== -->
	<!-- search results best bets info template -->
	<!-- ===================================================================== -->

	<xsl:template match="TopBestBets">

		<xsl:if test="BBTitle != ''">
			<h2 class="thunderstone-result-heading">
				<xsl:value-of select="BBTitle" disable-output-escaping="yes"/>
			</h2>
		</xsl:if>
		<dl class="best-bets, search-result">

			<xsl:for-each select="BestBet">
				<dt>
					<h3 class="result-title">
						<a href="{BBLink}">
							<xsl:copy-of select="BBResult/child::node()"/>
						</a>
					</h3>
				</dt>

				<xsl:if test="BBDescription != ''">
					<dd>
						<span class="result-abstract">
							<xsl:value-of select="BBDescription" disable-output-escaping="yes"/>
						</span>
					</dd>
				</xsl:if>

				<xsl:if test="BBLinkDisplay != ''"></xsl:if>
			</xsl:for-each>
		</dl>
		<h3 class="thunderstone-result-heading">Other Results</h3>
	</xsl:template>

	<!-- ===================================================================== -->
	<xsl:template match="RightBestBets">

		<xsl:if test="BBTitle!=''">
			<xsl:value-of select="BBTitle" disable-output-escaping="yes"/>
		</xsl:if>
		<div class='best-bets'>

			<xsl:for-each select="BestBet">

				<xsl:value-of select="BBResultNum"/>
				<a href="https://search.wwu.edu{BBLink}">
					<xsl:value-of select="BBResult" disable-output-escaping="yes"/>
				</a>

				<xsl:if test="BBDescription != ''">
					<xsl:value-of select="BBDescription" disable-output-escaping="yes"/>
				</xsl:if>

				<xsl:if test="BBLinkDisplay != ''"></xsl:if>
			</xsl:for-each>
		</div>
	</xsl:template>

	<!-- ===================================================================== -->
	<xsl:template name="InFieldQueries">

		<xsl:if test="InFieldQueriesAllowed='Y'">

			<xsl:for-each select="InFieldQuery">
				<h3>With this field:</h3>
				<input size="30" id="infq{@field}" name="infq" value="{Query}"/>
			</xsl:for-each>
			<!-- one more blank for user to add a query: -->
			<h3>With this field:</h3>
			<input size="30" name="infq" value=""/>
		</xsl:if>
	</xsl:template>

	<!-- ===================================================================== -->
	<xsl:template match="ParametricQuery">
		<!-- Parametric Search SQL and parameter input boxes. -->

		<xsl:if test="UserQueryAllowed='Y'">
			<h3>Parametric Search SQL:</h3>
			<input size="30" name="psq" value="{Query}"/>
		</xsl:if>

		<xsl:for-each select="Param">

			<xsl:choose>

				<xsl:when test="@label != ''">
					<xsl:value-of select="@label"/>
				</xsl:when>

				<xsl:otherwise>

					<xsl:value-of select="substring(@name,3)"/>:
				</xsl:otherwise>
			</xsl:choose>
			<input size="30" name="{@name}" value="{.}"/>
		</xsl:for-each>
	</xsl:template>

	<!-- ===================================================================== -->
	<xsl:template match="GroupByResults">
		<!-- display the "header" for this GroupBy field -->
		<div class='group-by-results'>

			<xsl:value-of select="Field"/>

			<xsl:if test="@countIsEstimate = 'Y'">
				Est.
			</xsl:if>
			Documents
		</div>
		<!-- display the individual rows for this field -->

		<xsl:apply-templates select="Group[Count!='']"/>
	</xsl:template>

	<!-- matches an individiual GroupBy result, displaying the "name count" -->
	<xsl:template match="GroupByResults/Group">
		<div class='group-by-results'>
			<!-- For tooltip, use <Tooltip> if given, else <Value>: -->

			<xsl:variable name="title">

				<xsl:choose>

					<xsl:when test="Tooltip!=''">
						<xsl:value-of select="Tooltip"/>
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="Value"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<!-- For visible value, use <ValueDisplay> or <Value>: -->
			<xsl:variable name="disp">

				<xsl:choose>

					<xsl:when test="ValueDisplay!=''">
						<xsl:value-of select="ValueDisplay"/>
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="Value"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:choose>

				<xsl:when test="UrlResults != ''">
					<a href="{UrlResults}">
						<xsl:value-of select="$disp"/>
					</a>
				</xsl:when>

				<xsl:otherwise>
					<xsl:value-of select="$disp"/>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:choose>

				<xsl:when test="../@CountIsEstimate = 'Y'">
					<span class="count">
						~<xsl:value-of select="Count"/>
					</span>
				</xsl:when>

				<xsl:otherwise>
					<xsl:value-of select="Count"/>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>

	<!-- ===================================================================== -->
	<xsl:template match="SiteQuery">
		<div class="site-query">
			<a href="#noexist" class="GroupByX" onclick="clearField('sq')">
				[x]
				<!-- &#x2716;-->
			</a>
			<h3>Site Query</h3>

			<xsl:value-of select="."/>
		</div>
	</xsl:template>

	<!-- ===================================================================== -->
	<xsl:template match="GroupByCollapsed">
		<div class="psGroupBy psGroupByGroup">
			<a href="{UrlResults}" title="Expand {Field}">
				<xsl:value-of select="Field"/>
			</a>
		</div>
	</xsl:template>

	<!-- ===================================================================== -->
	<xsl:template name="displayGroupBySelected">

		<xsl:for-each select="InFieldQuery">
			<div class="display-group-by-selected">
				<a href="#noexist" class="GroupByX" onclick="clearField('infq{@field}')">
					[x]
					<!-- &#x2716;-->
				</a>

				<xsl:value-of select="@field"/>

				<!-- For tooltip, use <Tooltip> if given, else <Value>s: -->
				<xsl:variable name="title">

					<xsl:choose>

						<xsl:when test="Tooltip!=''">
							<xsl:value-of select="Tooltip"/>
						</xsl:when>

						<xsl:otherwise>
							<xsl:value-of select="Value"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<!-- For visible value, use <ValueDisplay> or <Value>: -->
				<xsl:variable name="disp">

					<xsl:choose>

						<xsl:when test="ValueDisplay!=''">
							<xsl:value-of select="ValueDisplay"/>
						</xsl:when>

						<xsl:otherwise>
							<xsl:value-of select="Value"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				<xsl:value-of select="$disp"/>

			</div>
		</xsl:for-each>
	</xsl:template>

	<!-- ===================================================================== -->
	<!-- search results summary info template -->
	<!-- ===================================================================== -->

	<xsl:template match="Summary">

		<xsl:variable name="userResultsHits" select="UserResultsNum"/>

		<xsl:if test="$userResultsHits &gt; 0">
			<div class='search-results-summary'>
				<span class='number-of-results'>

					<xsl:value-of select="Start"/>
					through

					<xsl:text/>

					<xsl:value-of select="End"/>
					of

					<xsl:text/>

					<xsl:value-of select="Total"/>

					<xsl:value-of select="CurOrder"/>
					<!-- Intentional space -->
					<xsl:text> </xsl:text>
					<!-- Intentional space -->
					<a href="{OrderLink}">
						<xsl:value-of select="OrderType"/>
					</a>
				</span>

				<div class='group-by-collapsed'>
					<xsl:apply-templates select="GroupByCollapsed"/>
				</div>

				<xsl:if test="FirstPage='0' or LastPage='0'">
					<div class='search-pager'>

						<xsl:choose>

							<xsl:when test="FirstPage='1'"></xsl:when>

							<xsl:otherwise>
								<a href="{PreviousLink}">
									<span class="previous">previous</span>
								</a>
							</xsl:otherwise>
						</xsl:choose>

						<xsl:for-each select="Pages">

							<xsl:choose>

								<xsl:when test="PageLink='NULL'">
									<span class="active-page">
										<xsl:value-of select="PageNumber"/>
									</span>
								</xsl:when>

								<xsl:otherwise>
									<a href="{PageLink}">
										<xsl:value-of select="PageNumber"/>
									</a>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>

						<xsl:choose>

							<xsl:when test="LastPage='0'">
								<a href="{NextLink}">
									<span class="next">next</span>
								</a>
							</xsl:when>

							<xsl:otherwise></xsl:otherwise>
						</xsl:choose>

					</div>
				</xsl:if>
			</div>
		</xsl:if>
	</xsl:template>

	<!-- ===================================================================== -->
	<!-- Search Results Template -->
	<!-- ===================================================================== -->

	<xsl:template name="WesternStyle" match="Result">

		<xsl:call-template name="ResultGroupBySite-Attribute"/>
		<dl class="search-result">
			<dt>
				<h3 class="result-title">
					<span class="result-number">
						<xsl:call-template name="ResultNum"/>
					</span>

					<xsl:call-template name="ResultTitle"/>
				</h3>
			</dt>
			<dd>
				<span class="result-abstract">
					<xsl:call-template name="ResultAbstract"/>
				</span>
			</dd>
		</dl>
	</xsl:template>

	<!-- ===================================================================== -->
	<!-- Output of various setting parameters -->
	<!-- ===================================================================== -->

	<xsl:template name="repeat">

		<xsl:param name="output"/>

		<xsl:param name="count"/>

		<xsl:if test="$count &gt; 0">

			<xsl:value-of select="$output"/>

			<xsl:call-template name="repeat">

				<xsl:with-param name="output" select="$output"/>

				<xsl:with-param name="count" select="$count - 1"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="ResultAbstract">
		<!-- rather than outputting directly with value-of, use copy-of == on their nodes (which includes both text and <b> elements) == so <Abstract> itself isn't printed -->

		<xsl:copy-of select="Abstract/child::node()"/>
	</xsl:template>

	<xsl:template name="ResultDepth">
		<xsl:value-of select="Depth"/>
	</xsl:template>

	<xsl:template name="ResultGroupBySite-Attribute">
		<!-- If GroupBySite is Y and this Result's 'hostname:port' is the same as the previous one, indent it. 'hostname:port' is extracted as everything between '://' and the first '/' in <Url>. -->

		<xsl:if test="$GroupBySite = 'Y' and  substring-before(substring-after(preceding-sibling::Result[1]/Url, '://'),'/') = substring-before(substring-after(Url, '://'),'/')">

			<xsl:attribute name="class">groupIndent</xsl:attribute>
		</xsl:if>
	</xsl:template>

	<xsl:template name="ResultGroupBySite-More">
		<!-- If GroupBySite is Y and the next Result's 'host:port' is different from ours, or if there is no next one, output the "more results" link. -->

		<xsl:if test="$GroupBySite = 'Y' and (count(following-sibling::Result) = 0 or substring-before(substring-after(following-sibling::Result[1]/Url, '://'),'/') != substring-before(substring-after(Url, '://'),'/'))">
			<a class="groupIndent" href="{UrlMoreResultsFromSite}">
				More results from

				<xsl:value-of select="SiteName"/>
			</a>
		</xsl:if>
	</xsl:template>

	<xsl:template name="ResultLink">

		<xsl:choose>

			<xsl:when test="ClickUrl != ''">
				<xsl:value-of select="ClickUrl"/>
			</xsl:when>

			<xsl:otherwise>
				<xsl:value-of select="Url"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="ResultModified">
		<xsl:value-of select="Modified"/>
	</xsl:template>

	<xsl:template name="ResultNum">
		<xsl:value-of select="Num"/>
	</xsl:template>

	<xsl:template name="ResultRank">

		<xsl:if test="PercentRank!=''">

			<xsl:value-of select="PercentRank"/>
			<xsl:text>%</xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template name="ResultSize">

		<xsl:value-of select="DocSize"/>
		<xsl:text>B </xsl:text>
	</xsl:template>

	<xsl:template name="ResultTitle">

		<xsl:variable name="ResultLink">
			<xsl:call-template name="ResultLink"/>
		</xsl:variable>
		<a href="{$ResultLink}">
			<!-- rather than outputting directly with value-of, use copy-of == on their nodes (which includes both text and <b> elements) == so <ResultTitle> itself isn't printed -->

			<xsl:copy-of select="ResultTitle/child::node()"/>
		</a>
	</xsl:template>

	<xsl:template name="ResultUrlDisplay">
		<!-- rather than outputting directly with value-of, use copy-of == on their nodes (which includes both text and <b> elements) == so <UrlDisplay> itself isn't printed -->

		<xsl:copy-of select="UrlDisplay/child::node()"/>
	</xsl:template>

	<!-- ===================================================================== -->
	<!-- overall context (match info) page template -->
	<!-- ===================================================================== -->

	<xsl:template match="ThunderstoneContext">

		<xsl:choose>

			<xsl:when test="ProfileInfo/LoginUrl != ''">
				<xsl:call-template name="LoginForm"/>
			</xsl:when>

			<xsl:otherwise>
				<xsl:call-template name="commonHeaders"/>
				<div>

					<xsl:call-template name="SearchForm"/>

					<xsl:apply-templates select="ContextResult"/>

					<xsl:call-template name="Messages"/>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="ContextResult">
		<div class='context-match'>
			Match Context and Document Information
		</div>

		<xsl:if test="/ThunderstoneContext/TextQueryHighlight/* != ''">
			These search terms are highlighted:

			<xsl:copy-of select="/ThunderstoneContext/TextQueryHighlight/child::node()"/>
		</xsl:if>

		<xsl:if test="Url!=''">

			<xsl:variable name="ResultLink">
				<xsl:call-template name="ResultLink"/>
			</xsl:variable>
			<span class="url">
				<a href="{$ResultLink}">
					<xsl:value-of select="UrlDisplay"/>
				</a>
			</span>
			<span class="depth">
				Depth:

				<xsl:value-of select="Depth"/>
			</span>
			<span class='categories'>
				Categories:

				<xsl:for-each select="RecordCategory">
					<xsl:value-of select="."/>
				</xsl:for-each>
			</span>
			<span class='title'>
				Title:

				<xsl:copy-of select="Title/child::node()"/>
			</span>

			<span class='description'>
				Description:

				<xsl:copy-of select="Description/child::node()"/>
			</span>

			<span class='keywords'>
				Keywords:

				<xsl:copy-of select="Keywords/child::node()"/>
			</span>

			<span class='meta-data'>
				Meta data:

				<xsl:copy-of select="Meta/child::node()"/>
			</span>

			<xsl:for-each select="u:*">

				<xsl:value-of select="local-name(.)"/>:<xsl:apply-templates select="./child::node()"/>
			</xsl:for-each>

			<!-- This, "u:*" will display custom fields as simple strings. -->
			<!-- You can replace this to display custom fields in a custom way. -->

			<xsl:for-each select="u:*">

				<xsl:value-of select="local-name(.)"/>:

				<xsl:apply-templates select="./child::node()"/>
			</xsl:for-each>
			Body:

			<xsl:copy-of select="Body/child::node()"/>
		</xsl:if>
	</xsl:template>

	<!-- ====================================================================== -->
	<!-- overall parent links page template -->
	<!-- ====================================================================== -->

	<xsl:template match="ThunderstoneLinks">
		<xsl:call-template name="commonHeaders"/>
		<div>

			<xsl:call-template name="SearchForm"/>

			<xsl:call-template name="LinksSummaryTop"/>

			<xsl:apply-templates select="Result"/>

			<xsl:call-template name="Messages"/>

			<xsl:call-template name="LinksSummaryBottom"/>
		</div>
	</xsl:template>

	<!-- ====================================================================== -->
	<!-- parent links top summary template -->
	<!-- ====================================================================== -->

	<xsl:template name="LinksSummaryTop">
		<div class='links-summary'>
			Documents with links to:

			<xsl:variable name="TargetTitle" select="//TargetTitle"/>

			<xsl:choose>

				<xsl:when test="$TargetTitle=''">
					<!-- no title, use url -->
					<a href="{//TargetUrl}">
						<xsl:value-of select="//TargetUrl"/>
					</a>
				</xsl:when>

				<xsl:otherwise>
					<a href="{//TargetUrl}">
						<xsl:value-of select="//TargetTitle"/>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>

	<!-- ====================================================================== -->
	<!-- parent links bottom summary template -->
	<!-- ====================================================================== -->

	<xsl:template name="LinksSummaryBottom">
		<div class='links-summary'>

			<xsl:variable name="UrlMoreLinks" select="//UrlMoreLinks"/>

			<xsl:if test="$UrlMoreLinks!=''">
				<a href="{UrlMoreLinks}">More Parent Links...</a>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template name="RedirectPage">
		<head>
			<meta http-equiv="refresh" content="0;url={ProfileInfo/RedirectUrl}"></meta>
		</head>
		<p>
			<xsl:apply-templates select="ProfileInfo/ExitReason"/>
		</p>
		<p>
			Redirecting to

			<xsl:value-of select="ProfileInfo/RedirectUrl"/>....
		</p>
	</xsl:template>

	<xsl:template name="DisplayExitReason" match="ExitReason">

		<xsl:choose>

			<xsl:when test=". = 'ResAuth-ExternalLoginRequired'">
				External login required.
			</xsl:when>

			<xsl:when test=". = 'ResAuth-CredentialsRequired'">
				Please log in to continue searching.
			</xsl:when>

			<xsl:when test=". = 'ResAuth-LoginIncorrect'">
				Login incorrect.
			</xsl:when>

			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="TestModeBanner">

		<xsl:if test="mode='admin'">
			<p>
				Test mode (<xsl:value-of select="Profile"/>) -
				<a href="{AdminUrl}">
					Back to Administration
				</a>
				-
				<a href="{MakeLiveUrl}">
					Make this appearance live
				</a>
			</p>
		</xsl:if>
	</xsl:template>

	<!-- ===================================================================== -->
	<xsl:template name="Messages">
		<!-- Print <message>s and (legacy) <QueryMessage>s. -->
		<!-- <QueryMessage>s are legacy <message type="user">s: -->

		<xsl:for-each select="QueryMessage">
			<p class='query-message'>

				<xsl:value-of select="."/>&#0160;
			</p>
		</xsl:for-each>

		<xsl:for-each select="Message">
			<!-- Search users see only type="user" messages and only text; -->
			<!-- admin users see all message types and also line/script/etc. -->
			<!-- All other messages hidden in comments to avoid distraction -->
			<!-- (but available for specific perusal via View Source): -->

			<xsl:choose>

				<xsl:when test="@type='user'">
					<!-- user-level msg -->
					<p class='user-message'>

						<xsl:value-of select="."/>&#0160;
					</p>
				</xsl:when>

				<xsl:when test="@type='info'">
					<p class="infoMessage">
						<xsl:value-of select="."/>
					</p>
				</xsl:when>

				<xsl:when test="@type='admin'">

					<xsl:if test="../mode='admin'">
						<!-- admin mode -->
						<p class='admin-message'>

							<xsl:value-of select="@code"/>&#0160;

							<xsl:value-of select="@script"/>:<xsl:value-of select="@line"/>:

							<xsl:value-of select="."/>&#0160;
						</p>
					</xsl:if>
				</xsl:when>

				<xsl:otherwise>
					<!-- Text inside an <xsl:comment> does not get -->
					<!-- HTML-escaped consistently (eg. testXSLT, FireFox), -->
					<!-- so use <span> no-display instead of comment when -->
					<!-- needed (ie. end-comment string present), to escape -->
					<!-- not only end-comment string but potentially other -->
					<!-- tags (which would otherwise be live in a <span>): -->

					<xsl:choose>

						<xsl:when test="contains(., '-->')">
							<span style="display: none">

								<xsl:value-of select="@code"/>&#160;<xsl:value-of select="@script"/>:<xsl:value-of select="@line"/>:&#160; Unknown type (<xsl:value-of select="@type"/>):&#160;

								<xsl:value-of select="."/>
							</span>
						</xsl:when>

						<xsl:otherwise>
							<!-- all on one line: whitespace is significant in xsl:comment? -->

							<xsl:comment>
								<xsl:value-of select="@code"/>&#160;<xsl:value-of select="@script"/>:<xsl:value-of select="@line"/>:&#160;<xsl:value-of select="."/>
							</xsl:comment>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	<!-- ===================================================================== -->
	<!-- Common Headers to allow for auto complete search queries -->
	<!-- ===================================================================== -->
	<xsl:template name="commonHeaders">
		<script type="text/javascript" src="/common/js/jquery-3.3.1.min.js"/>
		<script type="text/javascript" src="/common/js/jquery-ui-1.12.1.custom.min.js"/>
		<script type="text/javascript" src="/common/js/jquery.ui.touch-punch.min.js?scriptVer=23.0.0"/>
		<script type="text/javascript" src="/common/js/search.js?scriptVer=23.0.0"/>
		<link rel="stylesheet" type="text/css" href="/common/css/jquery-ui-themes-1.12.1/redmond/jquery-ui.min.css"/>
		<link rel="stylesheet" type="text/css" href="/common/css/search.css?scriptVer=23.0.0"/>
		<script type="text/javascript" src="https://www.wwu.edu/themes/contrib/ashlar/build/js/display-settings.js"/>
		<script type="text/javascript" src="https://www.wwu.edu/themes/contrib/ashlar/build/js/wwu-header.js"/>
	</xsl:template>
</xsl:stylesheet>
